/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_prime.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/08 18:30:33 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/10 13:55:07 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_is_prime(int nb)
{
	int	a;

	if (nb <= 1)
		return (0);
	else if (nb == 3)
		return (1);
	else if (nb % 2 == 0 || nb % 3 == 0)
		return (0);
	a = 5;
	while (a * a <= nb)
	{
		if (nb % a == 0 || nb % (a + 2) == 0)
			return (0);
		a += 6;
	}
	return (1);
}
