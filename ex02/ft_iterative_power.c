/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_power.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/08 17:21:44 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/08 17:36:41 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_iterative_power(int nb, int power)
{
	int	pow;
	int	t;

	if (power < 0)
		return (0);
	else if (power == 0)
		return (1);
	pow = power;
	t = nb;
	while (pow > 1)
	{
		t *= nb;
		pow--;
	}
	return (t);
}
