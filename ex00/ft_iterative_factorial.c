/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/08 15:18:50 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/10 10:35:18 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_iterative_factorial(int nb)
{
	int	facto;
	int	i;

	if (nb < 0)
		return (0);
	else if (nb == 0)
		return (1);
	facto = nb;
	i = nb - 1;
	while (i <= nb && i > 1)
	{
		facto *= i;
		i--;
	}
	return (facto);
}
