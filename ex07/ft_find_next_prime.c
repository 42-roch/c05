/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_next_prime.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/08 18:46:37 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/12 15:42:25 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	is_prime_number(int nb)
{
	int	a;

	if (nb <= 1)
		return (0);
	else if (nb == 3)
		return (1);
	else if (nb % 2 == 0 || nb % 3 == 0)
		return (0);
	a = 5;
	while (a * a <= nb)
	{
		if (nb % a == 0 || nb % (a + 2) == 0)
			return (0);
		a += 6;
	}
	return (1);
}

int	ft_find_next_prime(int nb)
{	
	int	prime;

	if (nb <= 2)
		return (2);
	else if (nb == 3)
		return (3);
	else if (is_prime_number(nb) == 1)
		return (nb);
	prime = nb;
	while (prime < 2 * nb)
	{
		if (is_prime_number(prime))
			return (prime);
		prime++;
	}
	return (prime);
}
